package pl.kamildaniluk.paperscissorrock.services;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import pl.kamildaniluk.paperscissorrock.exceptions.NoWinnerException;
import pl.kamildaniluk.paperscissorrock.model.Choice;
import pl.kamildaniluk.paperscissorrock.model.Player;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.jupiter.api.Assertions.*;

class GameTest {

    private final int NUMBER_OF_GAMES = 100;
    private final PrintStream standardOut = System.out;
    private final ByteArrayOutputStream outputStreamCaptor = new ByteArrayOutputStream();
    private final Game game = new Game(NUMBER_OF_GAMES);

    @BeforeEach
    public void setUp() {
        System.setOut(new PrintStream(outputStreamCaptor));
    }

    @AfterEach
    public void tearDown() {
        System.setOut(standardOut);
    }

    @Test
    void checkRequiredParams() {
        Player player1 = new Player("Player A");
        Player player2 = new Player("Player B");
        player1.setChoice(Choice.ROCK);
        player2.setChoice(Choice.SCISSOR);

        assertDoesNotThrow(() -> game.checkRequiredParams(player1, player2));
    }

    @Test
    void checkRequiredParams_withMissingPlayer() {
        Player player1 = new Player("Player A");
        Player player2 = null;
        player1.setChoice(Choice.ROCK);

        Exception exception = assertThrows(IllegalArgumentException.class, () ->
                game.checkRequiredParams(player1, player2));
        String expectedMessage = "The game cannot be started. The required number of players has not been passed!";
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    void printPlayerStats() {
        Player player = new Player("PlayerA");
        player.win();

        game.printPlayerStats(player);

        String expectedResult = "Player PlayerA wins 1 of " + NUMBER_OF_GAMES + " games";

        assertEquals(expectedResult, outputStreamCaptor.toString().trim());
    }

    @Test
    void printTieStats() {
        game.printTieStats(21);

        String expectedResult = "Tie: 21 of " + NUMBER_OF_GAMES + " games";

        assertEquals(expectedResult, outputStreamCaptor.toString().trim());
    }

    @Test
    void checkWhoWonTheDuel() throws NoWinnerException {
        Player player1 = new Player("Player A");
        Player player2 = new Player("Player B");
        player1.setChoice(Choice.ROCK);
        player2.setChoice(Choice.SCISSOR);

        assertEquals(player1, game.checkWhoWonTheDuel(player1, player2));
    }

    @Test
    void checkWhoWonTheDuelInCaseOfTie() {
        Player player1 = new Player("Player A");
        Player player2 = new Player("Player B");
        player1.setChoice(Choice.PAPER);
        player2.setChoice(Choice.PAPER);

        Exception exception = assertThrows(NoWinnerException.class, () ->
                game.checkWhoWonTheDuel(player1, player2));
        String expectedMessage = "The duel cannot be resolved!";
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
    }


    @Test
    void checkWhoWonTheWholeGame() {
        Player player1 = new Player("Player A");
        Player player2 = new Player("Player B");

        player1.win();
        player1.win();
        player2.win();
        player2.win();
        player2.win();

        assertEquals(player2, game.checkWhoWonTheWholeGame(player1, player2));
    }
}
