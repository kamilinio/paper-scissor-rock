package pl.kamildaniluk.paperscissorrock.model;

import org.junit.jupiter.api.Test;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;

class PlayerTest {

    @Test
    void createUser() {
        Player player1 = new Player("Player1");
        Player player2 = new Player("Player2");
        assertEquals("Player1", player1.getUserName());
        assertEquals(UUID.fromString("a77da550-27d2-3a0d-93b7-aa0b161daca7"), player1.getUuid());
        assertEquals("Player2", player2.getUserName());
        assertEquals(UUID.fromString("35e65843-30a7-3e7e-9eb9-2b4bf44b1aad"), player2.getUuid());
    }

}
