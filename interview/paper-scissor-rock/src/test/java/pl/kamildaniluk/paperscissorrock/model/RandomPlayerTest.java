package pl.kamildaniluk.paperscissorrock.model;

import org.junit.jupiter.api.Test;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

class RandomPlayerTest {

    @Test
    void createUser() {
        Player player1 = new RandomPlayer("Computer");
        assertEquals("Computer", player1.getUserName());
        assertEquals(UUID.fromString("181900da-d960-3ecc-b34f-53c4e0ff4647"), player1.getUuid());
    }

    @Test
    void setRandomChoice() {
        RandomPlayer randomPlayer = new RandomPlayer("Computer");
        final Choice randomPlayerChoice = randomPlayer.getChoice();
        assertNotNull(randomPlayerChoice);
    }

}
