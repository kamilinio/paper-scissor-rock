package pl.kamildaniluk.paperscissorrock.model;

import lombok.Getter;
import lombok.Setter;

import java.util.UUID;

@Getter
public class Player {
    private final UUID uuid;
    private final String userName;
    @Setter
    private Choice choice;
    private int duelsWon;

    public Player(String userName) {
        this.uuid = UUID.nameUUIDFromBytes(userName.getBytes());
        this.userName = userName;
    }

    public void win() {
        this.duelsWon++;
    }

    public void resetDuelsWon() {
        this.duelsWon = 0;
    }
}
