package pl.kamildaniluk.paperscissorrock.exceptions;

public class NoWinnerException extends Exception {
    public NoWinnerException(String message) {
        super(message);
    }
}
