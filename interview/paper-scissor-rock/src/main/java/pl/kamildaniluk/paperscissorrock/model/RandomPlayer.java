package pl.kamildaniluk.paperscissorrock.model;

import java.util.Random;

public class RandomPlayer extends Player {

    private final Random random = new Random();

    public RandomPlayer(String userName) {
        super(userName);
    }

    @Override
    public Choice getChoice() {
        randomChoice();
        return super.getChoice();
    }

    private void randomChoice() {
        int ordinal = random.nextInt(3);
        setChoice(Choice.values()[ordinal]);
    }
}
