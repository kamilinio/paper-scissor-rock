package pl.kamildaniluk.paperscissorrock.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import pl.kamildaniluk.paperscissorrock.exceptions.NoWinnerException;
import pl.kamildaniluk.paperscissorrock.model.Choice;
import pl.kamildaniluk.paperscissorrock.model.Player;

import java.util.Objects;

@Component
public class Game {


    private final int NUMBER_OF_GAMES;

    @Autowired
    public Game(@Value("${psr.numberOfGames:100}") int numberOfGames) {
        this.NUMBER_OF_GAMES = numberOfGames;
    }

    public void play(Player player1, Player player2) throws IllegalArgumentException {
        checkRequiredParams(player1, player2);
        int tie = 0;

        for (int i = 0; i < NUMBER_OF_GAMES; i++) {
            try {
                Player winner = checkWhoWonTheDuel(player1, player2);
                winner.win();
            } catch (NoWinnerException ex) {
                tie++;
            }
        }
        showSummary(player1, player2, tie);
    }

    private void showSummary(Player player1, Player player2, int tie) {
        showGameStatistics(player1, player2, tie);
        showInfoAboutTheWinner(player1, player2, tie);
    }

    private void showInfoAboutTheWinner(Player player1, Player player2, int tie) {
        Player winner = checkWhoWonTheWholeGame(player1, player2);
        if (Objects.nonNull(winner)) {
            System.out.printf("Winner is %s (%d to %d wins)",
                    winner.getUserName(), winner.getDuelsWon(), (NUMBER_OF_GAMES - winner.getDuelsWon() - tie));
        } else {
            System.out.println("There is no winner! Draw");
        }
    }

    void checkRequiredParams(Player player1, Player player2) throws IllegalArgumentException {
        if (Objects.isNull(player1) || Objects.isNull(player2)) {
            throw new IllegalArgumentException(
                    "The game cannot be started. The required number of players has not been passed!");
        }
    }

    private void showGameStatistics(Player player1, Player player2, int tie) {
        printPlayerStats(player1);
        printPlayerStats(player2);
        printTieStats(tie);
    }

    void printTieStats(int tie) {
        System.out.printf("Tie: %d of %d games\n", tie, NUMBER_OF_GAMES);
    }

    void printPlayerStats(Player player) {
        System.out.printf("Player %s wins %d of %d games\n",
                player.getUserName(), player.getDuelsWon(), NUMBER_OF_GAMES);
    }

    Player checkWhoWonTheDuel(Player player1, Player player2) throws NoWinnerException {
        if (player1.getChoice() == player2.getChoice()) {
            throw new NoWinnerException("The duel cannot be resolved!");
        } else if (player1.getChoice() == Choice.PAPER) {
            return (player2.getChoice() == Choice.ROCK) ? player1 : player2;
        } else if (player1.getChoice() == Choice.SCISSOR) {
            return (player2.getChoice() == Choice.PAPER) ? player1 : player2;
        } else {
            return (player2.getChoice() == Choice.SCISSOR) ? player1 : player2;
        }
    }

    Player checkWhoWonTheWholeGame(Player player1, Player player2) {
        if (player1.getDuelsWon() == player2.getDuelsWon()) {
            return null;
        } else {
            return (player1.getDuelsWon() > player2.getDuelsWon()) ? player1 : player2;
        }
    }
}
