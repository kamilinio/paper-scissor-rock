package pl.kamildaniluk.paperscissorrock.model;

public enum Choice {
    PAPER,
    SCISSOR,
    ROCK
}
