package pl.kamildaniluk.paperscissorrock.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import pl.kamildaniluk.paperscissorrock.model.Choice;
import pl.kamildaniluk.paperscissorrock.model.Player;
import pl.kamildaniluk.paperscissorrock.model.RandomPlayer;
import pl.kamildaniluk.paperscissorrock.services.Game;

@SpringBootApplication(scanBasePackages = "pl.kamildaniluk.paperscissorrock")
public class PaperScissorRockApplication {

    public static void main(String[] args) {
        ConfigurableApplicationContext context = SpringApplication.run(PaperScissorRockApplication.class, args);
        Game game = context.getBean(Game.class);

        Player player1 = new Player("PlayerA");
        player1.setChoice(Choice.PAPER);
        Player player2 = new RandomPlayer("Player B");

        game.play(player1, player2);
    }

}
